module ApplicationHelper
	def full_title(page_title = '')
		base_title = 'Modus'
		if page_title.empty?
			base_title
		else
			base_title + " | " + page_title
		end
	end

	def menu_active_make(title_content = '')
		script = ''
		if !title_content.empty?
			script += 'jQuery(".menu_bar ul.nav > li").each(function(){'
			script += 'jQuery(this).removeClass("active");'
			script += 'if(jQuery(this).children("a").text() =="' + title_content + '"){' 	
			script += 'jQuery(this).addClass("active");'
			script +=	'}});'
		end
		script
	end
end
