jQuery(document).ready(function($){
	$("#owl-slider").owlCarousel({

    navigation : true, // Show next and prev buttons
    slideSpeed : 300,
    paginationSpeed : 400,
    singleItem:true

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false
  });
	$("#owl-slider").owlCarousel();
	$('.slider-1').flexslider();
	$('.slider-2').flexslider();
	$('.slider-3').flexslider();
	$('#clients_slider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 400,
    itemMargin: 20
  });
});